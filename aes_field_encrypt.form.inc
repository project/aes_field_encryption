<?php
/*
 * User password form sumbit function
 */
function _aes_field_encrypt_user_password($form, &$form_state) {
  //Get user name by orginal name or mail
  if (variable_get('aes_field_encrypt_user_name', FALSE) || variable_get('aes_field_encrypt_user_mail', FALSE)) {
    $form_state['values']['name'] = _aes_field_encrypt_db_get_name_by_mail($form_state['values']['name']);
  }
}

/*
 * Cope with encrypted name and mail validate issues
 */
function _aes_field_encrypt_register($form, &$form_state) {
  //Check if the user name is taoken
  if (variable_get('aes_field_encrypt_user_name', FALSE)) {
    if (_aes_field_encrypt_has_encrypted_value('users', 'name', $form_state['values']['name'])) {
      form_set_error('name', t('The name %name is already taken.', array('%name' => $form_state['values']['name'])));
    }
  }
  //Check if the mail is registered
  if (variable_get('aes_field_encrypt_user_mail', FALSE)) {
    if (_aes_field_encrypt_has_encrypted_value('users', 'mail', $form_state['values']['mail'])) {
      form_set_error('mail', t('The e-mail address %email is already registered. <a href="@password">Have you forgotten your password?</a>', array('%email' => $form_state['values']['mail'], '@password' => url('user/password'))));
    }
  }
}

/*
 * AES setting Form submit function
 */
function aes_field_encrypt_form_submit($form, &$form_state) {
  //Decrypt user name, if User Name Encrypt setting is off and user name are encrypted
  if ($form_state['values']['user_name'] !== 1) {
    if (variable_get('aes_field_encrypt_user_name', FALSE)) {
      _aes_field_encrypt_db_decrypt('users', 'name');
    }
  }
  //Encrypt user name, if User Name Encrypt setting is on and user name are not encrypted
  else {
    if (!variable_get('aes_field_encrypt_user_name', FALSE)) {
      _aes_field_encrypt_db_encrypt('users', 'name');
    }
  }
  //Decrypt user mail addres, if User Mail Encrypt setting is off and user mail are encrypted.
  if ($form_state['values']['user_mail'] !== 1) {
    if (variable_get('aes_field_encrypt_user_mail', FALSE)){
      _aes_field_encrypt_db_decrypt('users', 'mail');
    }
  }
  //Encrypt user mail address, if User Mail Encrypt setting is on and user mail are not encrypted.
  else {
    if (!variable_get('aes_field_encrypt_user_mail', FALSE)){
      _aes_field_encrypt_db_encrypt('users', 'mail');
    }
  }

  //Save the setting changes
  variable_set('aes_field_encrypt_user_name', $form_state['values']['user_name']);
  variable_set('aes_field_encrypt_user_mail', $form_state['values']['user_mail']);
  variable_set('aes_field_encrypt_name_placeholder', $form_state['values']['name_placeholder']);

  //After encrypting or decrypting user data , we need to flush the caches
  drupal_flush_all_caches();
}

/*
 * User login form validate function
 */
function _aes_field_encrypt_login($form, &$form_state) {
  
  $input_name = $form_state['values']['name'];
  
  /*
  if ( variable_get('aes_field_encrypt_user_mail', FALSE) && valid_email_address($input_name) ) {
    $form_state['values']['name'] = _aes_field_encrypt_db_get_user_mail($input_name);
  }
  */
  if (variable_get('aes_field_encrypt_user_name', FALSE)) {
    // Replace the user login name with the encrypted name
    $form_state['values']['name'] = _aes_field_encrypt_db_get_user_name($form_state['values']['name']);
  }
}

/*
 * Form validate function
 */
function _aes_field_encrypt_validate($form, &$form_state) {
global $user;
  $name = $form_state['values']['name'];
  $mail = $form_state['values']['mail'];
  $edit_user = $form['#user'];
  
  $edit_own_profile = ($user->uid === $edit_user->uid) ? TRUE : FALSE;

  if (variable_get('aes_field_encrypt_user_name', FALSE)) {
    $decrypted_user_name = _aes_field_encrypt_db_select_value('users', $edit_user->uid, 'name');
    
    //When user edit their own profiles, we do not need to check the user name if it wasn't changed. Otherwise, we will check if the new user name has been registered.
    if ($edit_own_profile) {
      if ($form_state['values']['name'] !== $decrypted_user_name) {
        if (_aes_field_encrypt_has_encrypted_value('users', 'name', $name)) {
          form_set_error('name', t('The name %name is already taken.', array('%name' => $form_state['values']['name'])));
        }
      }
      else {
        $form_state['values']['name'] = $decrypted_user_name;
       if (isset($form_state['values']['current_pass_required_values']['name'])) {
          unset($form_state['values']['current_pass_required_values']['name']);
        }
      }
    }
  }

  if (variable_get('aes_field_encrypt_user_mail', FALSE)) {
    $decrypted_user_mail = _aes_field_encrypt_db_select_value('users', $edit_user->uid, 'mail');
    
    //When user edit their own profiles, we do not need to check the user mail if it wasn't changed. Otherwise, we will check if the new mail address has been registered.
    if ($edit_own_profile) {
      if ($form_state['values']['mail'] !== $decrypted_user_mail) {   
        if (_aes_field_encrypt_has_encrypted_value('users', 'mail', $mail)) {
          if ($user->uid) {
            form_set_error('mail', t('The e-mail address %email is already taken.', array('%email' => $form_state['values']['mail'])));
          }
          else {
            form_set_error('mail', t('The e-mail address %email is already registered. <a href="@password">Have you forgotten your password?</a>', array('%email' => $form_state['values']['mail'], '@password' => url('user/password'))));
          }
        }
      }
      else {
        $form_state['values']['mail'] = $decrypted_user_mail;
        if (isset($form_state['values']['current_pass_required_values']['mail'])) {
          unset($form_state['values']['current_pass_required_values']['mail']);
        }
      }
    }
    //When changing other user profiles, we need to replace the email address with the decrypted user mail address if it is not changed.
    else {
      if ($form_state['values']['mail'] === $edit_user->mail) {
        $form_state['values']['mail'] = $decrypted_user_mail;
      }
    }
  }
}

/**
 * Validate aes_field_encrypt_notification_action form submissions.
 */
function aes_field_encrypt_notification_action_validate($form, $form_state) {
  $form_values = $form_state['values'];

  $selected_roles = array();

  foreach ($form_values['roles'] as $role) {
    if ($role !== 0) {
      $selected_roles[] = $role;
    }
  }
  // Validate the configuration form.
  if (empty($selected_roles)) {
    // We want the literal %author placeholder to be emphasized in the error message.
    form_set_error('recipient', t('Choose at least one role of users as the mail recipient'));
  }

}

/*
 * Notification sending form submit function
 */
function aes_field_encrypt_notification_action_submit($form, $form_state) {
  $form_values = $form_state['values'];
  //Put mail subject and body into a parameters array
  $params = array(
      //  'recipient' => $form_values['recipient'],
      'subject'   => $form_values['subject'],
      'message'   => $form_values['message'],
  );
  //Get all user ids who has specific user roles
  $uids = _aes_field_encrypt_role_recipients($form_values['roles']);
  $operations = array();

  //Create bath operations array and theirs parameters
  foreach ($uids as $uid) {
    $params['uid'] = $uid;
    $operations[] = array('_aes_field_encrypt_notification_process', $params);
  }

  //Define a Drupal bath operations array
  $batch = array(
      'title' => t('Send notification mail process'),
      'operations' => $operations,
      'finished' => '_aes_field_encrypt_notification_finished',
      'init_message' => t('Initializing...'),
      'progress_message' => t('Opertation @current out of @total.'),
      'error_message' => t('Found some error here.'),
  );
  //Run the bath operations
  batch_set($batch);

}

/*
 * The notification mail sending form
 */
function aes_field_encrypt_notification_action_form($settings, &$form_state) {
  $form = array();

  // Get a list of all roles, except for the anonymous user role.
  $allroles = db_select('role', 'r')
  ->fields('r', array('rid', 'name'))
  ->condition('rid', 1, '>')
  ->orderBy('name', 'ASC')
  ->execute();

  // Cycle through each role, adding it to the array to include in the selction
  // list.
  foreach ($allroles as $role) {
    $rolesarray[$role->rid] = $role->name;
  }

  $form['recipient'] = array('#type' => 'fieldset', '#title' => 'Recipients');

  // Create a set of checkboxes, including each role.
  $form['recipient']['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select user roles as the Recipients'),
      '#options' => $rolesarray,
      '#description' => t('These selected roles will be added to the mailing recipients. Note: if you check "authenticated user", other roles will not be added, as they will receive the email anyway.'),
  );
  $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#maxlength' => '254',
      '#description' => t('The subject of the message.'),
  );
  $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#cols' => '80',
      '#rows' => '20',
      '#description' => t('The message that should be sent. You may include placeholders like [node:title], [user:name], and [comment:body] to represent data that will be different each time message is sent. Not all placeholders will be available in all contexts.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Send to users'));

  $form['#submit'] = array('aes_field_encrypt_notification_action_submit');
  $form['#validate'] = array('aes_field_encrypt_notification_action_validate');

  return $form;
}

/**
 * Alter the field settings form.
 */
function _aes_field_encrypt_field_settings_alter(&$form, &$form_state, $form_id) {

  // Obtain the field name from form (should really be $form_state,
  // but the Date field doesn't comply).
  $field_name = $form['#field']['field_name'];

  // Try to obtain information about this field.
  $field_info = field_info_field($field_name);
  $field_type_info = $field_info['type'];
 
  // If this field type cannot be encrypted, exit here
  if (substr($field_type_info, 0, 4) !== 'text') {
    return;
  }

  $fld_settings =& $field_info['settings'];

  $form_state['aes_field_encrypt'] = array(
      'field_name' => $field_name,
      'field_info' => $field_info,
      'field_type_info' => $field_type_info,
      'encrypt' => isset($fld_settings['aes_field_encrypt']) ? $fld_settings['aes_field_encrypt']['encrypt'] : FALSE,
  );

  $settings =& $form['field']['settings'];

  $settings['aes_field_encrypt'] = array(
      '#type' => 'fieldset',
      '#title' => t('Encrypt this field by AES'),
      '#description' => t('Set up the parameters for encrypting this field.'),
      '#tree' => TRUE,
      'encrypt' => array(
          '#type' => 'checkbox',
          '#title' => t('Encrypt this field'),
          '#default_value' => $form_state['aes_field_encrypt']['encrypt'],
          '#weight' => 0,
      ),
      '#weight' => -2,
  );

  // Hide the option from non-privileged users,
  // but ensure the values carried through
  if (!user_access(AES_FIELD_ENCRYPT_PERMISSION)) {
    $field_encrypt =& $settings['aes_field_encrypt'];
    $field_encrypt['encrypt']['#type'] = 'value';
    $field_encrypt['encrypt']['#value'] = $field_encrypt['encrypt']['#default_value'];
  }


  // Add a submit handler
  $form['#submit'][] = '_aes_field_encrypt_field_settings_submit';
}

function _aes_field_encrypt_field_settings_submit($form, &$form_state) {
  $field_info = $form_state['aes_field_encrypt']['field_info'];
  $old_encrypt = $form_state['aes_field_encrypt']['encrypt'];

  $values =& $form_state['values']['field']['settings']['aes_field_encrypt'];
  $new_encrypt = $values['encrypt'];

  if ($old_encrypt) {
    if (!$new_encrypt) {
      // was encrypted but now unencrypted, so unencrypt it
      //module_load_include('inc', 'aes_field_encrypt');
      _aes_field_encrypt_decrypt_field($field_info);
    }
  }
  elseif ($new_encrypt) {
    // encryption flag wasn't set, but is now
   // module_load_include('inc', 'aes_field_encrypt');
    _aes_field_encrypt_field_encrypt($field_info);
  }
}