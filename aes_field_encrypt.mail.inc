<?php
/*
 * Implements hook_mail
 */
function aes_field_encrypt_mail($key, &$message, $params) {
  switch ($key) {
    case 'bath_action_send_notification_email':
      //Set the mail subject and body
      $message['subject'] = $params['subject'];
      $message['body'][] = $params['body'];
      break;
  }
}

/*
 * Get mail address by the placeholder name
 * @Parameters
 *   $address: placeholder mail address string. For example, Encrypted user mail-1, Encrypted user mail-2
 * @Return
 *   the real mail address string corresponding. For exmaple, admin@drupal.com, user1@drupal.com
 */
function _aes_field_encrypt_convert_mail_address($addresses) {
  $placeholder = variable_get('aes_field_encrypt_name_placeholder', 'Encrypted user') . ' mail-';
  $user_mails = explode(',', $addresses);
  $decrypted_mails = '';

  foreach ($user_mails as $mail) {
    $mail = trim($mail);
    if (strpos($mail, $placeholder) === 0) {
      $user_id = (int)substr($mail, strlen($placeholder));
      if ($user_id > 0 ) {
        $decrypted_mails .= _aes_field_encrypt_db_select_value('users', $user_id, 'mail') . ',';
      }
    }
    else {
      $decrypted_mails .= $mail . ',';
    }
  }

  return $decrypted_mails = empty($decrypted_mails) ? $addresses : $decrypted_mails;
}

/*
 * Get the user mail address by user ID.
 * @Parameters
 *   $uid: User ID
 * @Return
 *   Decrypted user mail address
 */
function _aes_field_encrypt_get_mail_by_uid($uid) {
  if ( empty($uid) || !is_int($uid)) {
    return NULL;
  }

  $result =  db_query("SELECT `value` FROM aes_field_encrypt WHERE `entity_type` = 'user' AND `bundle` = 'users' AND `field_name` = 'mail' AND entity_id = $uid");

  if ($result->rowCount() > 0) {
    return aes_decrypt($result->fetchObject()->value);
  }
  else {
    return NULL;
  }
}

/*
 * Fetch the user placehoder name by original name or mail
 * @Parameters:
 * $name: user login name or email
 */
function _aes_field_encrypt_db_get_name_by_mail($name) {
  $resutl = db_query("SELECT name FROM users, aes_field_encrypt WHERE entity_id = uid AND bundle = 'users' AND entity_type = 'user' AND field_name IN ('mail','name') AND `value` = :value", array(':value' => aes_encrypt($name)))->fetchObject();
  
  if (!empty($resutl->name)) {
    return $resutl->name;
  }
  else {
    return NULL;
  }
}