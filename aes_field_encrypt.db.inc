<?php
/*
 * Decrypt the specific field in database
 * @Parameters:
 *  $table: database table name
 *  $field_name: Field name in the table
 */
function _aes_field_encrypt_db_decrypt($table, $field_name) {

  $sql_text = "SELECT entity_id,`value` FROM aes_field_encrypt
  WHERE bundle = '$table' AND `field_name` = '$field_name'";

  switch ($table) {
    case 'users':
      $entity_id  = 'uid';
      break;
    case 'node':
      $entity_id = 'nid';
      break;
    default:
      $entity_id = 'entity_id';
  }

  $results =  db_query($sql_text);

  //Go through all records to decrypt the field value one by one.
  foreach ($results as $result) {
    //If there is no encrypted value, then skip this record.
    if (empty($result->value)) {
      continue;
    }

    $decrypted_value = aes_decrypt($result->value);
    //Skip this record, if decrypted value is empty
    if (!empty($decrypted_value)) {
      //Update the field value with decrypted one in the target database table
      db_update($table)
      ->fields(array($field_name => $decrypted_value))
      ->condition($entity_id, $result->entity_id)
      ->execute();
       
      //Delete decrypted record from aes_field_encrypt table
      db_delete('aes_field_encrypt')
      ->condition('entity_id', $result->entity_id)
      ->condition('bundle', $table)
      ->condition('field_name', $field_name)
      ->execute();
    }

  }

  drupal_set_message( $table . ' ' . $field_name . ' are decrypted.');
}

/*
 * Encrypt specific field value in a database table
 * @Parameters:
 *   $table: Database table name
 *   $field_name: Database field name
 */
function _aes_field_encrypt_db_encrypt($table, $field_name) {

  $placeholder = variable_get('aes_field_encrypt_name_placeholder', 'Encrypted user');
  switch ($table) {
    case 'users':
      $entity_id  = 'uid';
      break;
    case 'node':
      $entity_id = 'nid';
      break;
    default:
      $entity_id = 'entity_id';
  }

  //Fetch all data from the target table
  $results = db_select($table, 'u')
  ->fields('u')
  ->execute();

  //Go through all records to replace the field value with encrypted one
  foreach ($results as $result) {
    $encrypted_value = aes_encrypt($result->$field_name);

    if (!empty($encrypted_value)) {
      db_update($table)
      ->fields(array($field_name => $placeholder . ' ' . $field_name . '-' . $result->uid))
      ->condition($entity_id, $result->uid)
      ->execute();
      //Insert new record to encrypt field table
      db_insert('aes_field_encrypt')
      ->fields(array('entity_type' => 'user', 'bundle' => $table, 'entity_id' => $result->uid, 'field_name' => $field_name, 'language' => $result->language, 'value' => $encrypted_value))
      ->execute();
    }
  }

  drupal_set_message( $table . ' ' . $field_name . ' are encrypted.');
}

/*
 * Update encrypted field value in database
 */
function _aes_field_encrypt_db_update_value($bundle, $entity_id, $field_name, $value, $entity_type = 'user', $language = NULL, $unique = TRUE) {
  $value = aes_encrypt($value);
  //If it is a unique field and there is a duplicate value in database, return false
  if( $unique && db_query("SELECT entity_id FROM aes_field_encrypt WHERE `entity_type` = '$entity_type' AND `bundle` = '$bundle' AND `value` = '$value' AND `field_name` = '$field_name'")->rowCount() > 0) {
    return FALSE;
  }

  //Update the old value in databse, if it already exists.
  if (db_query("SELECT entity_id FROM aes_field_encrypt WHERE `entity_type` = '$entity_type' AND `bundle` = '$bundle' AND `entity_id` = $entity_id AND `field_name` = '$field_name'")->rowCount() > 0) {
    $num_updated = db_update('aes_field_encrypt')
    ->condition('entity_id', $entity_id)
    ->condition('bundle', $bundle)
    ->condition('field_name', $field_name)
    ->condition('$entity_type', $entity_type)
    ->fields(array(
        'value' => $value
    ))
    ->execute();
  }
  //Otherwise, insert a new record.
  else {
    db_insert('aes_field_encrypt')
    ->fields(array('entity_type' => $entity_type, 'bundle' => $bundle, 'entity_id' => $entity_id, 'field_name' => $field_name, 'value' => $value))
    ->execute();

    $placeholder = variable_get('aes_field_encrypt_name_placeholder', 'Encrypted user');
    db_update($bundle)
    ->fields(array($field_name => $placeholder . ' ' . $field_name . '-' . $entity_id))
    ->condition('uid', $entity_id)
    ->execute();
  }

  return TRUE;
}

/*
 * Fetch the decrypted value
 * @Parameters:
 * $bundle: bundle name
 * $entity_id: Entity ID
 * $field_name: Field name
 * $entity_type: Entity type
 * $language: Language
 */
function _aes_field_encrypt_db_select_value($bundle, $entity_id, $field_name, $entity_type = 'user', $revision_id = NULL, $language = NULL) {
    
  
  $query = db_select('aes_field_encrypt')
  ->fields('aes_field_encrypt')
  ->condition('entity_id', $entity_id)
  ->condition('bundle', $bundle)
  ->condition('field_name', $field_name)
  ->condition('$entity_type', $entity_type);
  
  if (!empty($language)) {
      $query->condition('language', array($language, 'und'), 'IN');
  }
  
  if (!empty($revision_id)) {
      $query->condition('revision_id', $revision_id);
  }
  
  $resutl = $query->execute()->fetchObject();

  if (empty($resutl->value)) {
    return NULL;
  }

  return aes_decrypt($resutl->value);
}




/*
 * Check if there is a value encrypted
 * @Parameters:
 * $bundle: bundle name
 * $entity_id: Entity ID
 * $field_name: Field name
 * $entity_type: Entity type
 * $language: Language
 */
function _aes_field_encrypt_has_encrypted_value($bundle,  $field_name, $value, $entity_type = 'user', $language = NULL) {
  $value = aes_encrypt($value);

  $resutl = db_select('aes_field_encrypt')
  ->fields('aes_field_encrypt')
  ->condition('value', $value)
  ->condition('bundle', $bundle)
  ->condition('field_name', $field_name)
  ->condition('$entity_type', $entity_type)
  ->execute()->fetchObject();

  if (empty($resutl->value)) {
    return FALSE;
  }

  return TRUE;
}


/*
 * Fetch the encrypted user name correspnoding to the original name
 * @Parameters
 * $name: the original user name
 *
 * Return:
 * The encrypted user name
 */
function _aes_field_encrypt_db_get_user_name($name) {

  $aes_name = aes_encrypt($name);

  $result =  db_query("SELECT `users`.name FROM aes_field_encrypt,users WHERE `entity_type` = 'user' AND `bundle` = 'users' AND `field_name` = 'name' AND `value` = '$aes_name' AND `users`.uid = `aes_field_encrypt`.entity_id");

  if ($result->rowCount() > 0) {
    return $result->fetchObject()->name;
  }
  else {
    return $name;
  }
}

/*
 * Fetch the encrypted user mail correspnoding to the user mail placehoder
 * @Parameters
 * $name: the original user name
 *
 * Return:
 * The encrypted user name
 */
function _aes_field_encrypt_db_get_user_mail($name) {

  $aes_name = aes_encrypt($name);

  $result =  db_query("SELECT `users`.mail FROM aes_field_encrypt,users WHERE `entity_type` = 'user' AND `bundle` = 'users' AND `field_name` = 'mail' AND `value` = '$aes_name' AND `users`.uid = `aes_field_encrypt`.entity_id");

  if ($result->rowCount() > 0) {
    return $result->fetchObject()->mail;
  }
  else {
    return $name;
  }
}


/**
 * Retrieves a list of users by roles.
 *
 * Gets the user IDs for all users in all roles included in the category.
 *
 * @param array $recipients
 *   The list of items for this category. For this plugin implementation, it is
 *   an array of role IDs.
 *
 * @return array
 *   The user IDs that are part of specified roles.
 */
function _aes_field_encrypt_role_recipients($recipients) {
  // Check to see if a role has been selected.
  if (!is_array($recipients)) {
    return;
  }

  $result = array();
  // Select the users.
  if (in_array('2', $recipients)) {
    // If the selected role is the authenticated users role, get everyone.
    $query = "SELECT u.uid FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid WHERE status = 1";
    $result = db_query($query)->fetchAllAssoc('uid', PDO::FETCH_ASSOC);
  }
  elseif (!empty($recipients)) {
    // Get the list of users who belong to the current role.
    $query = "SELECT u.uid FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid WHERE ur.rid IN (:ur_rids) AND status = 1";
    $result = db_query($query, array(':ur_rids' => $recipients))->fetchAllAssoc('uid', PDO::FETCH_ASSOC);
  }
  if (empty($result)) {
    return;
  }

  // Collect the uids.
  $uids = array();
  foreach ($result as $record) {
    // For each record in the result set, add the user's ID to the array.
    $uids[] = (integer) $record['uid'];
  }

  return $uids;
}

/*
 * Delete user encryted profile
 */
  function _aes_field_encrypt_delete_user($uid) {
    db_delete('aes_field_encrypt')
    ->condition('entity_id', $uid)
    ->condition('bundle', 'users')
    ->condition('$entity_type', 'user')
    ->execute();
  }
  
  /**
  * Decrypt a field.
  */
  function _aes_field_encrypt_decrypt_field($field_info) {
    $base_fields = array('entity_type', 'bundle', 'entity_id', 'revision_id', 'language', 'delta');
  
    $entities = array();
  
    $field_name = $field_info['field_name'];
    $results = db_select('aes_field_encrypt', 'afe')
    ->fields('afe')
    ->condition('afe.field_name', $field_name)
    ->orderBy('afe.entity_id', 'ASC')
    ->orderBy('afe.revision_id', 'DESC') // Get the current revision first.
    ->execute();
  
    // Fetch all records from the encrypt table.
    foreach ($results as $result) {
      $fields = $base_fields;
      $insert = array();
      foreach ($fields as $field) {
        $insert[$field] = $result->$field;
      }
  
      $value = aes_decrypt($result->value);

      foreach ((array) unserialize($value) as $key => $val) {
        $insert[$key] = $val;
        $fields[] = $key;
      }
  
      // Break down data into entity ids so we can control what's revision and what's current data.
      $entities[$result->entity_id][] = $insert;
    }
  
    if (!empty($entities)) {
      // Build the multiple insert query and do it.
      $data_query = db_insert("field_data_$field_name")->fields($fields);
      $revision_query = db_insert("field_revision_$field_name")->fields($fields);
  
      foreach ($entities as $revisions) {
        // Insert the current revision only.
        $data_query->values($revisions[0]);
  
        // Insert all revisions into the revision table.
        foreach ($revisions as $revision) {
          $revision_query->values($revision);
        }
      }
  
      $data_query->execute();
      $revision_query->execute();
  
      // Remove rows from the AES field encrypt table.
      db_delete('aes_field_encrypt')->condition('field_name', $field_name)->execute();
    }
  
    drupal_set_message(t('%field_name is no longer being encrypted', array('%field_name' => $field_name)));
  }
  
  /*
   * Encrypt a field and delete it from field_data table
   */
  function _aes_field_encrypt_field_encrypt($field_info) {
    $fields = array('field_name', 'entity_type', 'bundle', 'entity_id', 'revision_id', 'language', 'delta', 'value');
    $loop_fields = array('entity_type', 'bundle', 'entity_id', 'revision_id', 'language', 'delta');
  
    $inserts = array();
  
    $field_name = $field_info['field_name'];
    $data_table = 'field_data_' . $field_name;
    $revision_table = 'field_revision_' . $field_name;
  
    // Get all the current entries from the revision table.
    $results = db_select($revision_table, 't')->fields('t')->execute();
  
    // For each result build the insert data for field_encrypt.
    foreach ($results as $result) {
      $insert = array(
          'field_name' => $field_name,
      );
  
      foreach ($loop_fields as $field) {
        $insert[$field] = $result->$field;
        unset($result->$field);
      }
  
      // Encrypt field data
      $insert['value'] = aes_encrypt(serialize($result)); 
      $inserts[] = $insert;
    }
  
    if (!empty($inserts)) {
      // Build the multiple insert query and do it.
      $query = db_insert('aes_field_encrypt')->fields($fields);
      foreach ($inserts as $insert) {
        $query->values($insert);
      }
      $query->execute();
  
      // Remove rows from data and revision tables.
      db_delete($data_table)->execute();
      db_delete($revision_table)->execute();
    }
  
    drupal_set_message(t('%field_name is now being encrypted', array('%field_name' => $field_name)));
  }
  
  /*
   * Get decrypted field data of a entity specified by entity ID
   * @Parameters
   *   $field_name: The field name;
   *   $entity_type: The type of the entity;
   *   $id: The ID of the entity;
   *   $vid: The revision ID of the entity;
   * @ Return
   *   The decrypted field data array
   */
  function _aes_field_encrypt_get_decrypted_field_data( $field_name, $entity_type, $id, $vid) {
        $results = db_select('aes_field_encrypt', 'fe', array('fetch' => PDO::FETCH_ASSOC))
      ->fields('fe', array('language', 'delta', 'value'))
      ->condition('fe.field_name', $field_name)
      ->condition('fe.entity_type', $entity_type)
      ->condition('fe.entity_id', $id)
      ->condition('fe.revision_id', $vid)
      ->orderBy('fe.language', 'ASC')
      ->orderBy('fe.delta', 'ASC')
      ->execute();
      
      $field_data = array();
      $index = strlen($field_name) + 1;
      
      foreach ($results as $result) {
        // decrypt the encrypted value
        $value = aes_decrypt($result['value']);

        $item = array();
        foreach ((array) unserialize($value) as $key => $val) {
          $item[substr($key, $index)] = $val;
        }
        
        $field_data[$result['language']][$result['delta']] = $item;
      }
      
      return $field_data;
  }