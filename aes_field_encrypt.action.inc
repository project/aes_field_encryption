<?php
/**
 * The batch processor.
 */
function _aes_field_encrypt_notification_process($subject, $message, $uid, &$context) {
  // Do heavy lifting here...
  // Display a progress message...
  $context['message'] = "Now sending mail to user(" . $uid . ")...";
  //Get the user mail address
  $recipient = _aes_field_encrypt_get_mail_by_uid($uid);
  //Assign the mail parameters array
  $params = array(
      'subject' => $subject,
      'body' => $message,
  );

  if( !is_null($recipient) ) {
    $context['results'][] = drupal_mail('aes_field_encrypt', 'bath_action_send_notification_email', $recipient, language_default(), $params);
  }
}

/*
 * Notification mails sending operation finish handle function
 */
function _aes_field_encrypt_notification_finished($success, $results, $operations) {

  if ($success) {
    //Display the number of mails sent
    drupal_set_message(t('@count mails  processed.', array('@count' => count($results))));
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}

/*
 function aes_field_encrypt_notification_action($entity, $context) {
 dpm($entity);
 dpm($context);
 if (empty($context['node'])) {
 $context['node'] = $entity;
 }

 $recipient = token_replace($context['recipient'], $context);

 // If the recipient is a registered user with a language preference, use
 // the recipient's preferred language. Otherwise, use the system default
 // language.
 $recipient_account = user_load_by_mail($recipient);
 if ($recipient_account) {
 $language = user_preferred_language($recipient_account);
 }
 else {
 $language = language_default();
 }
 $params = array('context' => $context);


 if (drupal_mail('system', 'action_send_email', $recipient, $language, $params)) {
 watchdog('action', 'Sent email to %recipient', array('%recipient' => $recipient));
 }
 else {
 watchdog('error', 'Unable to send email to %recipient', array('%recipient' => $recipient));
 }


 }
 */